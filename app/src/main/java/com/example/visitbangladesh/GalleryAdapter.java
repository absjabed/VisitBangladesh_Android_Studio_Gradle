/*
 * To change this template, choose Tools | Templates
 * && open the template in the editor.
 */
package com.example.visitbangladesh;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;

import android.widget.ImageView;
import android.widget.BaseAdapter;


public class GalleryAdapter extends BaseAdapter {

    private final int[] thumbnails = new int[]{
        R.drawable.curry_view_thn,
        R.drawable.jai_thn
    };

    private final int[] images = new int[]{
        R.drawable.curry_view,
        R.drawable.jai
    };

    public int getCount() {
        return thumbnails.length;
    }

    public Object getItem(final int index) {
        return Integer.valueOf(images[index]);
    }

    public long getItemId(final int index) {
        return images[index];
    }

    public View getView(
            final int index,
            final View reuse,
            final ViewGroup parent) {

        final ImageView view = (reuse instanceof ImageView)
                ? (ImageView)reuse
                : (ImageView)LayoutInflater.from(parent.getContext()).inflate(
                R.layout.gallery_thn,
                null);

        view.setImageResource(images[index]);

        return view;
    }

}
