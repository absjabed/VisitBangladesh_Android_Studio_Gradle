package com.example.visitbangladesh;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements LocationListener {
	protected LocationManager locationManager;
	protected LocationListener locationListener;
	protected Context context;
	public Button bt1,bt2,bt3,bt4,bt5,bt6;
	public TextView txtgps;
	 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt1 = (Button) findViewById(R.id.btnInfo);
        bt2 = (Button) findViewById(R.id.btnSafety);
        bt3 = (Button) findViewById(R.id.btnCall);
        bt4 = (Button) findViewById(R.id.btnSMS);
        bt5 = (Button) findViewById(R.id.btnFeedback);
        bt6 = (Button) findViewById(R.id.abtBD);
        txtgps = (TextView) findViewById(R.id.gpsco);
        
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        
      bt1.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View view) {
			// TODO Auto-generated method stub
			Intent intent = new Intent(MainActivity.this, Divitions.class);
			
			
		    startActivity(intent);
			
		}
	});
      
      bt2.setOnClickListener(new View.OnClickListener() {
  		
  		@Override
  		public void onClick(View view) {
  			// TODO Auto-generated method stub
  			Intent intent = new Intent(MainActivity.this, Safeiss.class);
  			
  		    startActivity(intent);
  			
  		}
  	});
      bt3.setOnClickListener(new OnClickListener() {
    	  
			@Override
			public void onClick(View v) {

				Intent callintent = new Intent(Intent.ACTION_CALL);
				callintent.setData(Uri.parse("tel:123"));
				startActivity(callintent);

			}

		});
      
      bt4.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			String smsnumber = "01676749372";
			String smstxt = "Help Me I am in denger. My Current GPS Location is: '"+txtgps.getText().toString()+"'";
			
			Uri uri = Uri.parse("smsto:"+smsnumber);
			Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
			intent.putExtra("sms_body", smstxt);
			startActivity(intent);
			
		}
	});
      
      
      bt6.setOnClickListener(new View.OnClickListener() {
  		
  		@Override
  		public void onClick(View view) {
  			// TODO Auto-generated method stub
  			Intent intent = new Intent(MainActivity.this, AbtBD.class);
  			
  			
  		    startActivity(intent);
  			
  		}
  	});
      
      
        
    }
    
    @Override
    public void onLocationChanged(Location location) {
            
    	txtgps = (TextView) findViewById(R.id.gpsco);
		txtgps.setText("Latitude:" + location.getLatitude() + ", Longitude:"
				+ location.getLongitude());
    }
 
    @Override
    public void onProviderDisabled(String provider) {
         
        /******** Called when User off Gps *********/
         
        Toast.makeText(getBaseContext(), "Gps Service currently off", Toast.LENGTH_LONG).show();
    }
 
    @Override
    public void onProviderEnabled(String provider) {
         
        /******** Called when User on Gps  *********/
         
        Toast.makeText(getBaseContext(), "Gps turned on ", Toast.LENGTH_LONG).show();
    }
 
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
         
    }

}
