package com.example.visitbangladesh;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class Safeiss extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_safeiss);
		
		Button bt1 = (Button)findViewById(R.id.button1);
		   bt1.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View view) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(Safeiss.this, SaferoutA.class);
					
					
				    startActivity(intent);
					
				}
			});
			Button bt2 = (Button)findViewById(R.id.button2);
			   bt2.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View view) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(Safeiss.this, TipsA.class);
						
						
					    startActivity(intent);
						
					}
				});
				Button bt3 = (Button)findViewById(R.id.button3);
				   bt3.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View view) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(Safeiss.this, In_vihi.class);
							
							
						    startActivity(intent);
							
						}
					});
				   
				   Button bt4 = (Button)findViewById(R.id.button4);
				   bt4.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View view) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(Safeiss.this, CurrencyEA.class);
							
							
						    startActivity(intent);
							
						}
					});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.safeiss, menu);
		return true;
	}

}
